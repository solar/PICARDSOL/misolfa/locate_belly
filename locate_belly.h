//**************************locate_belly.h************************************
// Purpose:
// --------
// Locate Sun's belly position on MISOLFA images
// for precise guiding in real time
//
// History:
// -------
// V1.0 21/05/2009
//
// Authors:
// -------
// T. Corbard & C. Renaud 
// OCA/Cassiopee (corbar@oca.eu)
//
//*****************************************************************************

#ifndef _LOCATE_BELLY_H
#define _LOCATE_BELLY_H
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>

#define Calloc(n,type) (type *)calloc(n,sizeof(type))
#define Nearint(x)  (int)((x)+0.5-((x)<0.))



//=============================================================================
void locate_belly(unsigned short **im,int Nl,int Nc,float Rsun,int ws,int Nav, 
		  int step,int *xc, int *yc);

// input
// -----
// im,Nl,Nc : imput image im[Nl][Nc]  
//            Nl is the number of lines in the image, Nc the number of colums  
//            for MISOLFA Nl=480  Nc=640 
//
// Rsun: Apparent solar radius IN PIXELS at  observation time
//       close to 960/0.2 for MISOLFA
//
// ws: width of smoothing window when performing runing means: MUST BE ODD 
//       (anything between 15 and 41 seems to work fine: recommanded: 21
//
// Nav: Number of line averaged to produce the reduced image
//           e.g. If Nl=480, Nav=10 will produce a 48x640 reduced image
//      recommanded: 10    
//
// step: If step=1 we will consider all the (48) lines of the reduced image
//       otherwise, if step=2, we will consider one line over two etc..
//       high step will reduce computing time but also stability of the result.
//       
// Output
// ------
// [xc,yc] Left image belly coordinates in pixels with respect 
//         to the lower left corner of the window
//         xc is the horizontal coordinate, yc the vertical coordinate
//         yc can be <0 or >Nl-1 if belly is outside the image.
//=============================================================================

//=============================================================================
void locate_belly_1(unsigned short im1[],int Nl,int Nc,float Rsun, int ws,
		    int Nav, int step, int *xc, int *yc);
//
//Same as locate_belly but the input image is stored in a 1D array
//im1[..first line..,..second line..,..third line...]

//=============================================================================

//********************************END******************************************
#endif
