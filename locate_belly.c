//**************************locate_belly.c*************************************
// Purpose:
// --------
// Locate Sun's belly position on MISOLFA images
// for precise guiding in real time
//
// History:
// -------
// V1.0 21/05/2009
//
// Authors:
// -------
// T. Corbard & C. Renaud 
// OCA/Cassiopee (corbar@oca.eu)
//
//*****************************************************************************

#include "locate_belly.h"


//=============================================================================
static void grad (float  x[], int Nx, float Gx[]){

    //Compute the gradient of vector x using 2 point central difference
    // left end right differences are used at the edges

    int i;

    Gx[0]=x[1]-x[0];
    for (i=1;i<Nx-1;i++)Gx[i]=(x[i+1]-x[i-1])/2.0f; 
    Gx[Nx-1]=x[Nx-1]-x[Nx-2];
    
}
//=============================================================================

//=============================================================================
static float mean(float y[], int Ny)
{
    //get the average of vector y 

    float res;
    int i;

    for (res=0.,i=0; i<Ny;i++) res+=y[i];
	
    res/=Ny;

    return res;
}
//=============================================================================
//=============================================================================
static float mean2(float *y[], int Ny, int step)
{
    //get the average of the floats pointed by the elements of y[] 
    //taking every element (step=1) or one over two (step=2) etc..

    float res;
    int i,n;

    n=(Ny-1)/step+1; //number of elements averaged

    for (res=0.,i=0; i<Ny;i+=step) res+=*(y[i]);
	
    res/=n;

    return res;
}

//=============================================================================
//=============================================================================
static void run_mean_true (float x[], int Nx,int width, int step, float xm[])
{
    //Take a centered running mean over 'width' points.
    //Edges (i.e. width/2 points each side[0,..width/2-1][Nx-width/2,..Nx-1])
    //are duplicated each side.
    //Step defines wether all points of the window are taken (step=1)  
    //or one over two (step=2) etc..

    //LIMITATION: width must be odd

    int w2;
    int i;
    float **y;

    //width must be odd
    assert((width % 2) == 1);

    w2=width/2;
    y=Calloc(Nx+(2*w2),float*); //array of pointers to float
                                //will point to elements of x
    

    for  (i=0; i<w2; i++) y[i]=&x[w2-i-1]; //w2 first elements mirrowed
     
    for  (i=0; i<Nx; i++) y[i+w2]=&x[i]; 
 
    for  (i=0; i<w2; i++) y[Nx+w2+i]=&x[Nx-i-1];//w2 last elements mirrowed
 
    for (i=0; i<Nx; i++) xm[i]=mean2(y+i,width,step);
 
    free(y); 

}

//=============================================================================

//=============================================================================
static void run_meanv(unsigned short **A ,int Nl, int Nc, int Nav, int step, 
		  int NBl, int NBc, float *db, float **B){

    //On output each line of matrix B[NBl][Nc] is the average 
    //of Nav lines of matrix A[Nl][Nc].

    //if step=1 NBl=Nl/Nav and the first line of B(step=1) is the average
    //of the Nav first lines of A, the second line of B is the average
    //of the Nav next lines of A etc...(if Nl%Nav/=0 the remaining lines
    //are ignored)

    //If step/=1 B is a subset of the result we would have got using step=1.
    //The 1st line of B(step/=1) corresponds to the line ib=((Nl/Nav-1)%step)/2
    //of B(step=1). The line i of B(step/=1) corresponds to the line ib+i*step
    //of B(step=1) (i=0..NBl)
   
    //NBl gives the number of lines of B Nbl=(Nl/Nav-1)/step+1

    //db[NBl] gives the coordinate (0< <Nl) of each line of B in the frame 
    //of the input matrix A.
    
    //REQUIREMENTS: NBl==(Nl/Nav-1)/step+1 
    //              NBc==Nc

    //LIMITATION if Nl%Nav /= 0 the reminding lines are ignored

    int i,j,ib,kk,Nl_rdi,is;
   

    //Nunber of lines of the reduced image if step=1
    Nl_rdi=Nl/Nav;

    //Number of lines of the reduced image that will be kept
    assert(NBl==(Nl_rdi-1)/step+1);

    //number of colums for B
    assert(NBc==Nc);

    //First line of the reduced image used to fill B
    ib=((Nl_rdi-1)%step)/2;

    for (kk=0;kk<NBl;kk++){
	is=(ib+kk*step)*Nav;
	db[kk]=is+Nav/2.0f;
	for (j=0; j<NBc; j++){ 
	    for(i=0;i<Nav;i++)B[kk][j]+=A[is+i][j];
	    B[kk][j]/=Nav;
	}
    }
  
}
//=============================================================================

//=============================================================================
static float slope(float a[], float b[], int N){

    //linear regression coefficient
    //if b[i]=p*a[i]+c this function returns a
    //least square  estimate of p

    float ma,mb,mab,maa,p;
    float * y;
    int i;
 
    ma=mean(a,N);
    mb=mean(b,N);
  
    y=Calloc(N,float);

    for (i=0;i<N;i++) y[i]=a[i]*b[i];
 
    mab=mean(y,N);
 
    for (i=0;i<N;i++)y[i]=a[i]*a[i];

    maa=mean(y,N);

    p=(mab-(ma*mb))/(maa-(ma*ma));

    free(y);

    return(p);
}
 
//=============================================================================

//=============================================================================
static void minmaxloc(float v[], int N, int *imin, int *imax)
{
    //Get the locations (indexes) of the minimum and maximum
    //values within vector v.

    //LIMITATION: If the maximum or minimum value is present more than once
    //only the index of the first one is returned.

    int i;
    float min,max;

    for(min=v[0],max=v[0],*imin=0,*imax=0,i=1;i<N;++i){
	if (v[i]<min){
	    min=v[i];
	    *imin=i;
	}
	if (v[i]>max){
	    max=v[i];
	    *imax=i;
	}
    }
}
//=============================================================================

//=============================================================================
static void get_dist(float v[], int Nv,int rm_width, int *imin, int *imax)
{
    //Get the position (index) of the minimum and maximum gradients of vector v
    //A running mean of with 'rm_width' and using only one point over two 
    //in the window is performed before the gradient is calculated.
    //The resulting gradient is further smoothed by applying again
    //a running mean of width 'rm_width' using all the points of the window.

 
    float *vs,*G;


    //smooth the original vector by applying a running mean
    //when computing the mean only points with either odd or even indexes
    //are considered (v[i] and v[i+1] are never averaged together).
    vs=Calloc(Nv,float);
    run_mean_true(v,Nv,rm_width,2,vs);

    //Compute the gradient using two points central differences
    //Gx[i]=(v[i+1]-v[i-1])/2.
    G=Calloc(Nv,float);
    grad(vs,Nv,G);

    //smooth the resulting gradient by applying a running mean
    run_mean_true(G,Nv,rm_width,1,vs);

    //locate min and max of smoothed gradient
    minmaxloc(vs,Nv,imin,imax);

    free(vs);
    free(G);
}
    

//=============================================================================

//=============================================================================
static unsigned short **conv2D(unsigned short A[], int Nl, int Nc){
    
    //can be used to convert from 1D convention to 2D
    // B=conv2D(A,Nl,Nc)
    // then B[i][j]=A[i*Nc+j]   i=0,Nl-1  j=0,Nc-1
    // ADVANTAGE: MANIPULATE POINTERS ONLY: NO EXTRA MEMORY USED
    // AND NO DATA IS MOVED IN MEMORY.
    // WARNING: IF A HAS BEEN ALLOCATED BY THE USER IT SHOULD NOT BE FREED
    // B JUST POINTS TO  A
    
    unsigned short  **B;
    int i;
    
    B=Calloc(Nl,unsigned short*);
    
    for(i=0;i<Nl;i++)B[i]=A+i*Nc; // &A[i*Nc]
    
    return(B);
}
//=============================================================================


//=============================================================================
void locate_belly(unsigned short **im,int Nl,int Nc,float Rsun,int ws,int Nav, 
		  int step,int *xc, int *yc){

// input
// -----
// im,Nl,Nc : imput image im[Nl][Nc]  
//            Nl is the number of lines in the image, Nc the number of colums  
//            for MISOLFA Nl=480  Nc=640 
//
// Rsun: Apparent solar radius IN PIXELS at  observation time
//       close to 960/0.2 for MISOLFA
//
// ws: width of smoothing window when performing runing means: MUST BE ODD 
//       (anything between 15 and 41 seems to work fine: recommanded: 21
//
// Nav: Number of line averaged to produce the reduced image
//           e.g. If Nl=480, Nav=10 will produce a 48x640 reduced image
//      recommanded: 10    
//
// step: If step=1 we will consider all the (48) lines of the reduced image
//       otherwise, if step=2, we will consider one line over two etc..
//       high step will reduce computing time but also stability of the result.
//        
// Output
// ------
// [xc,yc] Left image belly coordinates in pixels with respect 
//         to the lower left corner of the window
//         xc is the horizontal coordinate, yc the vertical coordinate
//         yc can be <0 or >Nl-1 if belly is outside the image.
  
    int Nl_rdi,Nc_rdi,k,i,imin,imax,closest_x,j,i_rdi;

    int ymin,ymax;
    float d,xcf,ycf;
    float *i_im,*b,*yc_line;
    float **reduced_im;

    //Nav cannot be bigger then Nl  
    assert(Nav<=Nl);

    //Number of lines of the reduced image 
    Nl_rdi=(Nl/Nav-1)/step+1;

    //number of colums: unchanged
    Nc_rdi=Nc;

    //Allocate reduced image
    reduced_im=Calloc(Nl_rdi,float*);
    for(i=0;i<Nl_rdi;i++)reduced_im[i]=Calloc(Nc_rdi,float);

    //vector giving the position of each line of the reduced image
    //with respect to the original image
    i_im=Calloc(Nl_rdi,float);

    //Computes the reduced image by averaging lines and gives the position
    //of each line of the reduced image with respect to the original image
    run_meanv(im,Nl,Nc,Nav,step,Nl_rdi,Nc_rdi,i_im,reduced_im);


    //Allocate vector used for linear fit
    b=Calloc(Nl_rdi,float);

    
    //get limb positions for each line of the reduced image 
    for(k=0; k<Nl_rdi; k++){
	get_dist(reduced_im[k],Nc_rdi,ws,&imin,&imax);
	b[k]=(i_im[k]*i_im[k]-Rsun*(imax-imin))/2.0f;
    }

    //estimated height position of the belly with respect to the bottom
    //line of the window
    ycf=slope(i_im,b,Nl_rdi);
    *yc=Nearint(ycf);
    
    if(*yc>=0 && *yc <= Nl-1){//belly is within the window

	//take an average of Nav lines around belly position
	ymin=*yc-Nav/2;
	ymax=*yc+(Nav-1)/2;  //=Nav/2 if Nav odd,  =Nav/2-1 otherwise
	
       //If yc too close from the edges we still use Nav lines but they 
       //are not well centered around yc
	if(ymin<0){
	    ymin=0;
	    ymax=Nav-1;
	}else{ 
	    if(ymax>Nl-1){
		ymax=Nl-1;
		ymin=Nl-Nav;
	    }
	}
	
	yc_line=Calloc(Nc,float);
      
	for(j=0;j<Nc;j++){
	    for (i=ymin;i<ymax;i++)yc_line[j]+=im[i][j];
	}
	
	//find limb position on average line
	get_dist(yc_line,Nc,ws,xc,&imax);
	free(yc_line);

    }else{ //belly outide window

	i_rdi=(*yc<0)?0:(Nl_rdi-1); //choose the closest line of reduced im
                                    //i.e. top or bottom line of reduced image
	//find limb position on the closest line
	get_dist(reduced_im[i_rdi],Nc_rdi,ws,&closest_x,&imax);

	//distance between closest line and belly y-position 
	d=(*yc)-i_im[i_rdi];

	//Compute belly x-position
	xcf=closest_x+d*d/(2.0f*Rsun);
	*xc=Nearint(xcf);
    }

    for(i=0;i<Nl_rdi;i++)free(reduced_im[i]);
    free(reduced_im);
    free(i_im);
    free(b);
}
//=============================================================================

//=============================================================================
void locate_belly_1(unsigned short im1[],int Nl,int Nc,float Rsun, int ws,
		    int Nav, int step, int *xc, int *yc){
//Same as locate_belly but the input image is stored in a 1D array
//im1[..first line..,..second line..,..third line...]
   
    unsigned short **im;
    
    im=conv2D(im1,Nl,Nc);
    
    locate_belly(im,Nl,Nc,Rsun,ws,Nav,step,xc,yc);

    free(im);
}
//=============================================================================

    
//********************************END******************************************
